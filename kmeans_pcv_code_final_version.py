
# coding: utf-8

# In[1]:

from sklearn.cluster import KMeans
import pandas as pd
import numpy as np


# In[23]:

data = pd.read_csv("/home/valiance/Anubha/AMEX/Dataset_Modelling_PCV_New.csv")


# In[3]:

print(data.columns)


# In[24]:

del data['id']
del data['pcv']
del data['account_name']
del data['product']
del data['segment']


# In[25]:

data['tenure'] = data['tenure'].fillna(1)
data['Is_Importer'] = data['Is_Importer'].fillna('NULL')
data['sic_code'] = data['sic_code'].fillna(0)
data['city'] = data['city'].fillna('NULL')
data['employee'] = data['employee'].fillna(0)


# In[26]:

data.info()


# In[27]:

kmeans_model = KMeans(n_clusters=200, random_state=1)


# In[28]:

new_data=pd.get_dummies(data)


# In[29]:

print(new_data.columns)


# In[30]:

kmeans_model.fit(new_data)


# In[55]:

print(len(kmeans_model.labels_))


# In[56]:

out2=kmeans_model.labels_


# In[57]:

np.savetxt("/home/valiance/Anubha/AMEX/cluster_labels_train.csv", out2, delimiter=',', header="id", comments="")


# In[31]:

test_data=pd.read_csv('/home/valiance/Anubha/AMEX/Dataset_modelling_master_v2.csv')


# In[32]:

print(test_data.columns)


# In[33]:

del test_data['id']
del test_data['pcv']
del test_data['account_name']
del test_data['product']
del test_data['customer']
del test_data['IsFortune500']
del test_data['segment']


# In[34]:

test_data['tenure'] = test_data['tenure'].fillna(1)
test_data['Is_Importer'] = test_data['Is_Importer'].fillna('NULL')
test_data['sic_code'] = test_data['sic_code'].fillna(0)
test_data['city'] = test_data['city'].fillna('NULL')
test_data['employee'] = test_data['employee'].fillna(0)


# In[35]:

test_data.info()


# In[36]:

new_test_data=pd.get_dummies(test_data)


# In[37]:

tem_test=new_test_data[new_data.columns]


# In[38]:

print new_data.info()


# In[39]:

print new_test_data.info()


# In[44]:

out = kmeans_model.predict(new_test_data)


# In[50]:

np.savetxt("/home/valiance/Anubha/AMEX/cluster_labels.csv", out, delimiter=',', header="id", comments="")

