
# coding: utf-8

# In[89]:



# In[11]:

from sklearn.cluster import KMeans

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

kmeans_model = KMeans(n_clusters=200, random_state=1)

data = pd.read_csv("/home/valiance/Anubha/AMEX/Dataset_Modelling_PCV_New.csv")
data.ix[data.pcv > 10000000, 'pcv'] = 10000000
#data.ix[data.Revenue > 10000000, 'Revenue'] = 1000000

data.ix[data.tenure.isnull() == True, 'tenure'] = 1
data.ix[data.id.isnull() == True, 'id'] = 1
data.ix[data.Is_Importer.isnull() == True, 'Is_Importer'] = 'Yes'
data.ix[data.sic_code.isnull() == True, 'sic_code'] = 1
data.ix[data.pcv.isnull() == True, 'pcv'] = 1
data.ix[data.Revenue.isnull() == True, 'Revenue'] = 1
data.ix[data.employee.isnull() == True, 'employee'] = 1

# Get only the numeric columns from games.
good_columns = data._get_numeric_data()

#good_columns

print(len(good_columns))

#good_columns

# Fit the model using the good columns.
kmeans_model.fit(good_columns)
# Get the cluster assignments.
labels = kmeans_model.labels_



print(len(labels))
labels


# In[90]:

from sklearn.metrics import silhouette_samples, silhouette_score
#good_columns
labels


# In[91]:

good_columns=good_columns.values


# In[92]:

print(type(good_columns))


# In[84]:

def clusterelement(clustNum, labels_array): #numpy 
    return np.where(labels_array == clustNum)[0]

#def ClusterIndicesComp(clustNum, labels_array): #list comprehension
    #return np.array([i for i, x in enumerate(labels_array) if x == clustNum])


# In[85]:

clusterelement(67, kmeans_model.labels_)


# In[88]:

good_columns


# In[86]:

good_columns[clusterelement(67, kmeans_model.labels_)]


# In[87]:

print good_columns[120],kmeans_model.labels_[120]


# In[96]:

kmeans_model.predict(test[columns])


# In[95]:

#good_columns['pcv']


# In[10]:

# Import the PCA model.
from sklearn.decomposition import PCA

# Create a PCA model.
pca_2 = PCA(2)
# Fit the PCA model on the numeric columns from earlier.
plot_columns = pca_2.fit_transform(good_columns)
# Make a scatter plot of each game, shaded according to cluster assignment.
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1], c=labels)
# Show the plot.
plt.show()

data.corr()["pcv"]


# Get all the columns from the dataframe.
columns = data.columns.tolist()
# Filter the columns to remove ones we don't want.
columns = [c for c in columns if c not in ["account_name","city","lob","segment","product","Is_Importer"]]

# Store the variable we'll be predicting on.
target = "pcv"


# In[25]:

# Import a convenience function to split the sets.
from sklearn.cross_validation import train_test_split

# Generate the training set.  Set random_state to be able to replicate results.
train = data.sample(frac=0.8, random_state=1)
# Select anything not in the training set and put it in the testing set.
test = data.loc[~data.index.isin(train.index)]
# Print the shapes of both sets.
print(train.shape)
print(test.shape)

columns


# In[27]:

# Import the linearregression model.
from sklearn.linear_model import LinearRegression

# Initialize the model class.
model = LinearRegression()
# Fit the model to the training data.
model.fit(train[columns], train[target])


# In[29]:

# Import the scikit-learn function to compute error.
from sklearn.metrics import mean_squared_error

# Generate our predictions for the test set.
predictions = model.predict(test[columns])

# Compute error between our test predictions and the actual values.
mean_squared_error(predictions, test[target])


# In[37]:

