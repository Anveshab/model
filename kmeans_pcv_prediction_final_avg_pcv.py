
# coding: utf-8

# In[146]:

from sklearn.cluster import KMeans
import pandas as pd
import numpy as np


# In[147]:

data = pd.read_csv("/home/valiance/Anubha/AMEX/Dataset_Modelling_PCV_New .csv")


# In[148]:

print(data.columns)


# In[149]:

data.info()


# In[150]:

del data['id']
del data['pcv']
del data['account_name']
del data['product']
del data['segment']


# In[151]:

data['tenure'] = data['tenure'].fillna(1)
data['Is_Importer'] = data['Is_Importer'].fillna('NULL')
data['sic_code'] = data['sic_code'].fillna(0)
data['city'] = data['city'].fillna('NULL')
data['employee'] = data['employee'].fillna(0)


# In[152]:

data.info()


# In[153]:

kmeans_model = KMeans(n_clusters=200, random_state=1)


# In[154]:

new_data=pd.get_dummies(data)


# In[155]:

print(new_data.columns)


# In[156]:

kmeans_model.fit(new_data)


# In[157]:

print(len(kmeans_model.labels_))


# In[158]:

out2=kmeans_model.labels_


# In[159]:

data.info()


# In[160]:

np.savetxt("/home/valiance/Anubha/AMEX/cluster_labels_train.csv", out2, delimiter=',', header="cluster_id", comments="")


# In[161]:

import pandas as pd

first = pd.read_csv('/home/valiance/Anubha/AMEX/cluster_labels_train.csv')
second = pd.read_csv('/home/valiance/Anubha/AMEX/Dataset_Modelling_PCV_New .csv')


# In[162]:

test3 = pd.concat([second, first], axis=1)
test3


# In[245]:

groupby_data = test3['pcv'].groupby(test3['cluster_id'])


# In[280]:

print(temp)


# In[277]:

temp = groupby_data.mean()


# In[283]:

temp[199]


# In[251]:

df = pd.DataFrame(groupby_data.mean())


# In[274]:

df


# In[232]:

dic={}
for i in range(0,1):
    dic[i]=groupby_data.mean()


# In[270]:

dic


# In[165]:

test_data=pd.read_csv('/home/valiance/Anubha/AMEX/Dataset_modelling_master_v2.csv')


# In[ ]:




# In[166]:

print(test_data.columns)


# In[167]:

del test_data['id']
del test_data['pcv']
del test_data['account_name']
del test_data['product']
del test_data['customer']
del test_data['IsFortune500']
del test_data['segment']


# In[168]:

test_data['tenure'] = test_data['tenure'].fillna(1)
test_data['Is_Importer'] = test_data['Is_Importer'].fillna('NULL')
test_data['sic_code'] = test_data['sic_code'].fillna(0)
test_data['city'] = test_data['city'].fillna('NULL')
test_data['employee'] = test_data['employee'].fillna(0)


# In[169]:

test_data.info()


# In[170]:

new_test_data=pd.get_dummies(test_data)


# In[171]:

tem_test=new_test_data[new_data.columns]


# In[172]:

print new_data.info()


# In[173]:

print new_test_data.info()


# In[174]:

out = kmeans_model.predict(new_test_data)


# In[178]:

np.savetxt("/home/valiance/Anubha/AMEX/cluster_labels.csv", out, delimiter=',', header="cluster_id", comments="")


# In[179]:

import pandas as pd

first1 = pd.read_csv('/home/valiance/Anubha/AMEX/Dataset_modelling_master_v2.csv')
second1 = pd.read_csv('/home/valiance/Anubha/AMEX/cluster_labels.csv')


# In[180]:

test4 = pd.concat([first1,second1], axis=1)
test4


# In[202]:

print(type(test4))


# In[285]:

for i in range(len(test4)):
    test4.loc[i,'Predicted_pcv']=temp[test4.loc[i,'cluster_id']]


# In[287]:

del test4['predicted_pcv']


# In[288]:

test4


# In[292]:

test4.to_csv('/home/valiance/Anubha/AMEX/pcv_prediction.csv')

