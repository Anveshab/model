
# coding: utf-8

# In[ ]:

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

data = pd.read_csv("/home/valiance/Desktop/Anvesha/Scaled_Amex _data.csv")
# Get all the columns from the dataframe.
#columns = data.columns.tolist()
# Filter the columns to remove ones we don't want.
#columns = [c for c in columns if c not in ["id", "pcv2","account_name","Unnamed: 0"]]

# Store the variable we'll be predicting on.
target = "pcv2"


# In[7]:

columns = ['employee2','Revenue2','tenure2']


# In[8]:

# Import a convenience function to split the sets.
from sklearn.cross_validation import train_test_split

# Generate the training set.  Set random_state to be able to replicate results.
train = data.sample(frac=0.8, random_state=1)
# Select anything not in the training set and put it in the testing set.
test = data.loc[~data.index.isin(train.index)]
# Print the shapes of both sets.
print(train.shape)
print(test.shape)
# Import the linearregression model.
from sklearn.linear_model import LinearRegression

# Initialize the model class.
model = LinearRegression()
# Fit the model to the training data.
model.fit(train[columns], train[target])


# Import the scikit-learn function to compute error.
from sklearn.metrics import mean_squared_error

# Generate our predictions for the test set.
predictions = model.predict(test[columns])
# Generate our predictions for the test set.
#predictions 
# Compute error between our test predictions and the actual values.
mean_squared_error(predictions, test[target])

#print(len(train[columns]))


# In[20]:

import math
mean = test['pcv2'].mean()
n = len(test)
# sst = (1/n)*(Sum(i=1..n) (data['pcv2']-mean))

print mean
pcv3 = pd.Series(test['pcv2'])
# print pcv3
pcv3 = (pcv3 - mean)*(pcv3 - mean)
sst = sum(pcv3)
sst


# In[12]:

model.coef_


# In[13]:

model.residues_


# In[14]:

model.get_params


# In[15]:

model.intercept_


# In[21]:

ssreg = (pcv3-predictions)*(pcv3-predictions)
ssreg1 =sum(ssreg)
ssreg1


# In[22]:

r_square = 1-(ssreg1/sst)
r_square

