
# coding: utf-8

# In[28]:



# In[11]:

from sklearn.cluster import KMeans


# In[12]:

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt


kmeans_model = KMeans(n_clusters=200, random_state=1)


# In[14]:

data = pd.read_csv("/home/valiance/Anubha/AMEX/Dataset_Modelling_PCV_New.csv")
#data.ix[data.pcv > 10000000, 'pcv'] = 10000000
#data.ix[data.Revenue > 10000000, 'Revenue'] = 1000000


# In[16]:

data


# In[29]:


data.ix[data.tenure.isnull() == True, 'tenure'] = data['tenure'].min()
#data.ix[data.id.isnull() == True, 'id'] = 1
#data.ix[data.Is_Importer.isnull() == True, 'Is_Importer'] = 'Yes'
data.ix[data.sic_code.isnull() == True, 'sic_code'] =data['sic_code'].min()
data.ix[data.pcv.isnull() == True, 'pcv'] = data['pcv'].min()
data.ix[data.Revenue.isnull() == True, 'Revenue'] = data['Revenue'].min()
data.ix[data.employee.isnull() == True, 'employee'] = data['employee'].min()


# In[30]:

cols_to_transform = [ 'Is_Importer', 'product', 'segment', 'lob', 'city']
data2 = pd.get_dummies( data,columns = cols_to_transform )
del data2['Is_Importer_No']
del data2['lob_Retail Trade']
#del data2['IsFortune500_False']
del data2['segment_\n']
del data2['city_Zurich']
del data2['product_Corporate Card']

#print df_with_dummies


# In[17]:

# In[6]:

# Get only the numeric columns from games.
good_columns = data._get_numeric_data()


# In[111]:

#good_columns

print(len(good_columns))
#good_columns
# Fit the model using the good columns.
kmeans_model.fit(good_columns)
# Get the cluster assignments.
labels = kmeans_model.labels_

print(len(labels))
labels


# In[31]:

data['pcv2']=(data['pcv']-data['pcv'].min())/(data['pcv'].max()-data['pcv'].min()) 
data['tenure2']=(data['tenure']-data['tenure'].min())/(data['tenure'].max()-data['tenure'].min()) 
data['Revenue2']=(data['Revenue']-data['Revenue'].min())/(data['Revenue'].max()-data['Revenue'].min()) 
data['sic_code2']=(data['sic_code']-data['sic_code'].min())/(data['sic_code'].max()-data['sic_code'].min()) 
data['employee2']=(data['employee']-data['employee'].min())/(data['employee'].max()-data['employee'].min()) 
del data['employee']
del data['Revenue']
del data['sic_code']
del data['tenure']
del data['pcv']


# In[32]:

data


# In[33]:

#good_columns['pcv']


# In[10]:

# Import the PCA model.
from sklearn.decomposition import PCA

# Create a PCA model.
pca_2 = PCA(2)
# Fit the PCA model on the numeric columns from earlier.
plot_columns = pca_2.fit_transform(good_columns)
# Make a scatter plot of each game, shaded according to cluster assignment.
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1], c=labels)
# Show the plot.
plt.show()


# In[124]:



# In[24]:

# Get all the columns from the dataframe.
#columns = data.columns.tolist()
# Filter the columns to remove ones we don't want.
#columns = [c for c in columns if c not in ["id", "pcv2","account_name"]]

# Store the variable we'll be predicting on.
target = "pcv2"


# In[25]:

# Import a convenience function to split the sets.
from sklearn.cross_validation import train_test_split

# Generate the training set.  Set random_state to be able to replicate results.
train = data.sample(frac=0.8, random_state=1)
# Select anything not in the training set and put it in the testing set.
test = data.loc[~data.index.isin(train.index)]
# Print the shapes of both sets.
print(train.shape)
print(test.shape)


# In[26]:



# In[27]:


# In[ ]:




# In[34]:

columns = ['employee2','Revenue2','tenure2','sic_code2']


# In[45]:

# Import the linearregression model.
from sklearn.linear_model import LinearRegression

# Initialize the model class.
model = LinearRegression()
# Fit the model to the training data.
model.fit(train[columns], train[target])


# Import the scikit-learn function to compute error.
from sklearn.metrics import mean_squared_error

# Generate our predictions for the test set.
predictions = model.predict(test[columns])
# Generate our predictions for the test set.
#predictions 
# Compute error between our test predictions and the actual values.
mean_squared_error(predictions, test[target])



#print(len(train[columns]))


# In[47]:

model.fit(train[columns], train[target])
model.coef_
model.residues_
model.score
model.copy_X
model.decision_function
model.fit
model.singular_
#data['pcv2']


# In[55]:

k=pd.Series(predictions)


# In[59]:

print(type(data['pcv2']))


# In[66]:

error= ((test['pcv2'] - k) / test['pcv2'])


# In[69]:

print(len(test['pcv2']))


# In[65]:

abs(error.mean())

